package obook.main;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import obook.source.model.Book;
import obook.source.model.Group;
import obook.source.model.Media;
import obook.source.model.MediaCategory;
import obook.source.model.MediaCondition;
import obook.source.model.MediaEntry;
import obook.source.model.MediaEntryReturnedAction;
import obook.source.report.ReportFactory;
import obook.source.report.ReportGenerator;
import obook.source.report.ReportType;
import obook.tosort.GroupReport;

public class App {

	private static Set<Media> media = new HashSet<Media>();

	public static void main(String[] args) {

		// ----------------------------------------------------------
		// Test Categories
		// ----------------------------------------------------------

		MediaCategory cMath = new MediaCategory("Mathe");
		MediaCategory cMath5 = new MediaCategory("Mathe 5", cMath);
		MediaCategory cMath6 = new MediaCategory("Mathe 6", cMath);
		MediaCategory cMath7 = new MediaCategory("Mathe 7", cMath);
		cMath.getSubCategories().add(cMath5);
		cMath.getSubCategories().add(cMath6);
		cMath.getSubCategories().add(cMath7);

		MediaCategory cDeutsch = new MediaCategory("Deutsch");
		MediaCategory cDeutsch5 = new MediaCategory("Deutsch 5", cDeutsch);
		MediaCategory cDeutsch6 = new MediaCategory("Deutsch 6", cDeutsch);
		MediaCategory cDeutsch7 = new MediaCategory("Deutsch 7", cDeutsch);
		cDeutsch.getSubCategories().add(cDeutsch5);
		cDeutsch.getSubCategories().add(cDeutsch6);
		cDeutsch.getSubCategories().add(cDeutsch7);

		MediaCategory cEnglisch = new MediaCategory("Englisch");
		MediaCategory cEnglisch5 = new MediaCategory("Englisch 5", cEnglisch);
		MediaCategory cEnglisch6 = new MediaCategory("Englisch 6", cEnglisch);
		MediaCategory cEnglisch7 = new MediaCategory("Englisch 7", cEnglisch);
		cEnglisch.getSubCategories().add(cEnglisch5);
		cEnglisch.getSubCategories().add(cEnglisch6);
		cEnglisch.getSubCategories().add(cEnglisch7);

		// ----------------------------------------------------------
		// Test Books Math
		// ----------------------------------------------------------

		Book mathAll1 = new Book("000", "Mathematik For All", "-", "-");
		mathAll1.getAuthors().add("Lukas");
		mathAll1.getAuthors().add("Hannah");
		mathAll1.getCategories().add(cMath);
		cMath.getMembers().add(mathAll1);
		media.add(mathAll1);

		Book math1 = new Book("001", "Mathematik 5", "Mathematik Lernen Klasse 5", "-");
		math1.getAuthors().add("Lukas");
		math1.getAuthors().add("Hannah");
		math1.getCategories().add(cMath5);
		cMath5.getMembers().add(math1);
		media.add(math1);

		Book math2 = new Book("002", "Mathematik 6", "Mathematik Lernen Klasse 6", "-");
		math2.getAuthors().add("Lukas");
		math2.getAuthors().add("Hannah");
		math2.getCategories().add(cMath6);
		cMath6.getMembers().add(math2);
		media.add(math2);

		Book math3 = new Book("003", "Mathematik 6 - 2 Edition", "Mathematik Lernen Klasse 6", "-");
		math3.getAuthors().add("Lukas");
		math3.getAuthors().add("Hannah");
		math3.getCategories().add(cMath6);
		cMath6.getMembers().add(math3);
		media.add(math3);

		Book math4 = new Book("004", "Mathematik 7", "Mathematik Lernen Klasse 7", "-");
		math4.getAuthors().add("Lukas");
		math4.getAuthors().add("Hannah");
		math4.getCategories().add(cMath7);
		cMath7.getMembers().add(math4);
		media.add(math4);

		// ----------------------------------------------------------
		// Test Books German
		// ----------------------------------------------------------

		Book german1 = new Book("005", "Deutsch 5", "Deutsch Lernen Klasse 5", "-");
		german1.getAuthors().add("Lukas");
		german1.getAuthors().add("Hannah");
		german1.getCategories().add(cDeutsch5);
		cDeutsch5.getMembers().add(german1);
		media.add(german1);

		Book german2 = new Book("006", "Deutsch 5 - 2 Edition", "Deutsch Lernen Klasse 5", "-");
		german2.getAuthors().add("Lukas");
		german2.getAuthors().add("Hannah");
		german2.getCategories().add(cDeutsch5);
		cDeutsch5.getMembers().add(german2);
		media.add(german2);

		Book german3 = new Book("007", "Deutsch 5 - 3 Edition", "Deutsch Lernen Klasse 5", "-");
		german3.getAuthors().add("Lukas");
		german3.getAuthors().add("Hannah");
		german3.getCategories().add(cDeutsch5);
		cDeutsch5.getMembers().add(german3);
		media.add(german3);

		Book german4 = new Book("008", "Deutsch 7", "Deutsch Lernen Klasse 7", "-");
		german4.getAuthors().add("Lukas");
		german4.getAuthors().add("Hannah");
		german4.getCategories().add(cDeutsch7);
		cDeutsch7.getMembers().add(german4);
		media.add(german4);

		// ----------------------------------------------------------
		// Test Books English
		// ----------------------------------------------------------

		Book english1 = new Book("009", "Englisch 5", "Englisch Lernen Klasse 5", "-");
		english1.getAuthors().add("Lukas");
		english1.getAuthors().add("Hannah");
		english1.getCategories().add(cEnglisch5);
		cEnglisch5.getMembers().add(english1);
		media.add(english1);

		Book english2 = new Book("010", "Englisch 6", "Englisch Lernen Klasse 6", "-");
		english2.getAuthors().add("Lukas");
		english2.getAuthors().add("Hannah");
		english2.getCategories().add(cEnglisch6);
		cEnglisch6.getMembers().add(english2);
		media.add(english2);

		Book english3 = new Book("011", "Englisch 7", "Englisch Lernen Klasse 7", "-");
		english3.getAuthors().add("Lukas");
		english3.getAuthors().add("Hannah");
		english3.getCategories().add(cEnglisch7);
		cEnglisch7.getMembers().add(english3);
		media.add(english3);

		Book english4 = new Book("012", "Englisch 7 - 2 Edition", "Englisch Lernen Klasse 7", "-");
		english4.getAuthors().add("Lukas");
		english4.getAuthors().add("Hannah");
		english4.getCategories().add(cEnglisch7);
		cEnglisch7.getMembers().add(english3);
		media.add(english4);

		// ----------------------------------------------------------
		// Testing
		// ----------------------------------------------------------

		MediaEntry e = new MediaEntry(math1, "101010");
		e.getSupplements().add(new MediaEntry(math2));
		e.setCondition(MediaCondition.CONDITION_OK);
		e.addAction(new MediaEntryReturnedAction(null, null, new Timestamp(System.currentTimeMillis())));
		// System.out.println(e.toString());
		// System.out.println(cMath.toString());
		// System.out.println(math1.toString());
		// System.out.println(math2.toString());

		Group g = new Group("Class 5", "-");
		
		Group ga = new Group("Class 5 A", "-");
		ga.setParent(g);
		
		Group gaa = new Group("Class 5 AA", "-");
		gaa.setParent(ga);
		gaa.getWhiteList().add(cDeutsch5);
		
		Group gb = new Group("Class 5 B", "-");
		gb.setParent(g);
		gb.getBlackList().add(cEnglisch5);
		
		ReportFactory fac = ReportGenerator.getReportfactory(ReportType.HTML);
		System.out.println(fac.generateHTMLReport(new GroupReport()));
		System.out.println(fac.generateHTMLReport(null));

	}

}
