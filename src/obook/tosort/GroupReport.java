
/*
 * (C) Copyright 2016 Lukas Kannenberg
 */

/**
 * 
 */
package obook.tosort;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import obook.source.report.Report;
import obook.source.target.account.Account;

/**
 * // TODO insert type description here
 *
 * @author lkannenberg
 * @since 2016-07-21
 * @version 0.0.1
 *
 */
@SuppressWarnings("unused")
public class GroupReport implements Report {

	/**
	 * The date that the report is designed for
	 */
	private Date date;
	
	/**
	 * Represents the members of the Group at that time
	 */
	private Set<Account> members = new HashSet<>();
	
	/**
	 * Represents the desired stock of the Group at that time
	 */
	private Set<MediaStockInfo> stockInfo = new HashSet<>();
	
}
