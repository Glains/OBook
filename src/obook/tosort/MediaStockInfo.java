package obook.tosort;

import obook.source.model.Media;

public class MediaStockInfo {

	private Media media;
	
	private int amount;
	
	private int desiredAmount;

	public MediaStockInfo(Media media, int amount, int desired) {
		super();
		this.media = media;
		this.amount = amount;
		this.desiredAmount = desired;
	}

	/**
	 * @return the media
	 */
	public Media getMedia() {
		return media;
	}

	/**
	 * @param media the media to set
	 */
	public void setMedia(Media media) {
		this.media = media;
	}

	/**
	 * @return the amount
	 */
	public int getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}

	/**
	 * @return the desiredAmount
	 */
	public int getDesiredAmount() {
		return desiredAmount;
	}

	/**
	 * @param desiredAmount the desiredAmount to set
	 */
	public void setDesiredAmount(int desiredAmount) {
		this.desiredAmount = desiredAmount;
	}
	
}
