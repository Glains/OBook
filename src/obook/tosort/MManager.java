package obook.tosort;

import java.util.Date;
import java.util.Set;

import obook.source.model.Media;

public interface MManager {

	/**
	 * 
	 * @param account
	 * @return
	 */
	public Set<Media> getSuggestedMediaForCurrentGroup(String account);

	/**
	 * Generates and returns information about the current Stock contained for a
	 * specific Group. This information can be used to evaluate how many Media
	 * of the same kind is needed at a specific date, how many additional Media
	 * need to be ordered and many other related information.
	 * 
	 * @param group the provided Group
	 * @param date the Date to generate information for
	 * @return
	 */
	public GroupReport getMediastockReport(String group, Date date);
	
}
