package obook.source.media.exeption;

public class MediaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MediaException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MediaException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public MediaException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MediaException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MediaException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	

}
