package obook.source.media.exeption;

public class MediaEntryExeption extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MediaEntryExeption() {
		super();
	}

	public MediaEntryExeption(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MediaEntryExeption(String message, Throwable cause) {
		super(message, cause);
	}

	public MediaEntryExeption(String message) {
		super(message);
	}

	public MediaEntryExeption(Throwable cause) {
		super(cause);
	}
	
	

}
