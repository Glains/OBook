/*
 * (C) Copyright 2016 Lukas Kannenberg
 */
package obook.source.model;

/**
 * 
 * // TODO insert type description here
 *
 * @author Lukas
 * @since 2016-07-16
 * @version 0.0.1
 *
 */
public class CompactDisk extends Media {

	// ----------------------------------------------------------
	// Static Fields
	// ----------------------------------------------------------

	/**
	 * The serial Version UID of this class
	 */
	private static final long serialVersionUID = -7757909570657357943L;

	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	/*
	 * The title of this CompaktDisk
	 */
	private String title;

	/*
	 * The subtitle of this CompaktDisk
	 */
	private String subTitle;

	/*
	 * The description of this Book
	 */
	private String description;

	/*
	 * The ImageInformation of this CompaktDisk
	 */
	private MediaImageInfo imageInfo;

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------

	/**
	 * TODO
	 *
	 * @param id
	 * @param title
	 * @param subTitle
	 * @param description
	 */
	public CompactDisk(String id, String title, String subTitle, String description) {
		super(id);
		this.title = title;
		this.subTitle = subTitle;
		this.description = description;
	}

	// ----------------------------------------------------------
	// Accessors
	// ----------------------------------------------------------

	/**
	 * Returns the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Returns the subTitle.
	 * 
	 * @return the subTitle
	 */
	public String getSubTitle() {
		return subTitle;
	}

	/**
	 * Sets the subTitle.
	 * 
	 * @param subTitle the subTitle to set
	 */
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	/**
	 * Returns the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the imageInfo.
	 * 
	 * @return the imageInfo
	 */
	public MediaImageInfo getImageInfo() {
		return imageInfo;
	}

	/**
	 * Sets the imageInfo.
	 * 
	 * @param imageInfo the imageInfo to set
	 */
	public void setImageInfo(MediaImageInfo imageInfo) {
		this.imageInfo = imageInfo;
	}

	// ----------------------------------------------------------
	// Other
	// ----------------------------------------------------------
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CompactDisk [id=");
		builder.append(id);
		builder.append(", title=");
		builder.append(title);
		builder.append(", subTitle=");
		builder.append(subTitle);
		builder.append(", description=");
		builder.append(description);
		builder.append("]");
		return builder.toString();
	}

}
