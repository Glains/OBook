
/*
 * (C) Copyright 2016 Lukas Kannenberg
 */

/**
 * 
 */
package obook.source.model.helper;

import java.util.HashSet;
import java.util.Set;

/**
 * // TODO insert type description here
 *
 * @author lkannenberg
 * @since 2016-07-20
 * @version 0.0.1
 *
 */
public class Filter<T> {

	public Set<T> whitelist(Set<T> entities, Set<T> whitelist) {
		if (entities == null || whitelist == null) {
			return new HashSet<>();
		}
		if (whitelist.isEmpty()) {
			return entities;
		}
		Set<T> result = new HashSet<>();
		for (T t : entities) {
			if (whitelist.contains(t)) {
				result.add(t);
			}
		}
		return result;
	}
	
	public Set<T> blacklist(Set<T> entities, Set<T> blacklist) {
		if (entities == null || blacklist == null) {
			return new HashSet<>();
		}
		if (blacklist.isEmpty()) {
			return entities;
		}
		Set<T> result = new HashSet<>();
		for (T t : entities) {
			if (!blacklist.contains(t)) {
				result.add(t);
			}
		}
		return result;
	}
}
