
/*
 * (C) Copyright 2016 Lukas Kannenberg
 */

/**
 * 
 */
package obook.source.model.helper;

import java.util.HashSet;
import java.util.Set;

import obook.source.model.MediaCategory;

/**
 * // TODO insert type description here
 *
 * @author lkannenberg
 * @since 19.07.2016
 * @version 0.0.1
 *
 */
public class MediaCategories {

	// ----------------------------------------------------------
	// Static Methods
	// ----------------------------------------------------------

	/**
	 * Returns a <code>Set</code> with all accessible MediaCategories. This
	 * includes the provided MediaCategory itself and all other parent
	 * MediaCategories.
	 *
	 * @param mediaCategory
	 *            the provided MediaCategory
	 * @return a <code>Set</code> with all accessible MediaCategories
	 */
	public static Set<MediaCategory> getAccessibleCategories(MediaCategory mediaCategory) {
		Set<MediaCategory> result = new HashSet<>();
		if (mediaCategory.getParentCategory() != null) {
			result.addAll(getAccessibleCategories(mediaCategory.getParentCategory()));
		}
		result.add(mediaCategory);
		return result;
	}
}
