
/*
 * (C) Copyright 2016 Lukas Kannenberg
 */

/**
 * 
 */
package obook.source.model.helper;

import java.util.Set;

import obook.source.model.Group;
import obook.source.model.MediaCategory;

/**
 * // TODO insert type description here
 *
 * @author lkannenberg
 * @since 2016-07-20
 * @version 0.0.1
 *
 */
public class Groups {

	public static Set<MediaCategory> whitelist(Group g, Set<MediaCategory> categories) {
		return new Filter<MediaCategory>().whitelist(categories, g.getWhiteList());
	}
	
	public static Set<MediaCategory> blacklist(Group g, Set<MediaCategory> categories) {
		return new Filter<MediaCategory>().blacklist(categories, g.getBlackList());
	}
}
