package obook.source.model;

import java.io.Serializable;
import java.sql.Timestamp;

import obook.source.target.account.Account;

public abstract class MediaAction implements Serializable {

	// ----------------------------------------------------------
	// Static Fields
	// ----------------------------------------------------------

	/**
	 * The serial Version UID of this class
	 */
	private static final long serialVersionUID = 6695742965904168529L;
	
	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	/*
	 * The Account who returned the Media
	 */
	protected Account caller;

	/*
	 * The timestamp
	 */
	protected Timestamp timestamp;

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------

	/**
	 * TODO
	 *
	 * @param timestamp
	 */
	protected MediaAction(Account caller, Timestamp timestamp) {
		super();
		setCaller(caller);
		setTimestamp(timestamp);
	}

	// ----------------------------------------------------------
	// Accessors
	// ----------------------------------------------------------

	/**
	 * Returns the caller.
	 * 
	 * @return the caller
	 */
	public Account getCaller() {
		return caller;
	}

	/**
	 * Sets the caller.
	 * 
	 * @param caller
	 *            the caller to set
	 */
	public void setCaller(Account caller) {
		this.caller = caller;
	}

	/**
	 * Returns the timestamp of this action.
	 * 
	 * @return the timestamp
	 */
	public Timestamp getTimestamp() {
		return timestamp;
	}

	/**
	 * Sets the timestamp for this action.
	 * 
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	// ----------------------------------------------------------
	// Other
	// ----------------------------------------------------------

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MediaAction [caller=");
		builder.append(caller);
		builder.append(", timestamp=");
		builder.append(timestamp);
		builder.append("]");
		return builder.toString();
	}

}
