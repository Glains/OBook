/*
 * (C) Copyright 2016 Lukas Kannenberg
 */
package obook.source.model;

import java.sql.Timestamp;

import obook.source.target.account.Account;

/**
 * // TODO insert type description here
 *
 * @author Lukas Kannenberg
 * @since 2016-07-17
 * @version 0.0.1
 *
 */
public class MediaEntryReturnedAction extends MediaAction {

	// ----------------------------------------------------------
	// Static Fields
	// ----------------------------------------------------------
	
	/**
	 * The serial Version UID of this class
	 */
	private static final long serialVersionUID = -7881714874550471927L;
	
	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	/*
	 * The Account who returned the Media
	 */
	private Account target;

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------

	/**
	 * TODO
	 *
	 * @param timestamp
	 */
	public MediaEntryReturnedAction(Account target, Account caller, Timestamp timestamp) {
		super(caller, timestamp);
		setTarget(target);
	}

	// ----------------------------------------------------------
	// Accessors
	// ----------------------------------------------------------

	/**
	 * Returns the target.
	 * 
	 * @return the target
	 */
	public Account getTarget() {
		return target;
	}

	/**
	 * Sets the target.
	 * 
	 * @param target the target to set
	 */
	public void setTarget(Account target) {
		this.target = target;
	}

	// ----------------------------------------------------------
	// Other
	// ----------------------------------------------------------

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MediaEntryReturnedAction [caller=");
		builder.append(caller);
		builder.append(", timestamp=");
		builder.append(timestamp);
		builder.append(", target=");
		builder.append(target);
		builder.append("]");
		return builder.toString();
	}

}
