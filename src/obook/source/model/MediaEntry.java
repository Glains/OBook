/*
 * (C) Copyright 2016 Lukas Kannenberg
 */
package obook.source.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import obook.source.target.account.Account;

/**
 * TODO Class Description
 *
 * @author Lukas Kannenberg
 * @since 2016-03-07
 * @version 0.0.6
 *
 */
public class MediaEntry implements Serializable {

	// ----------------------------------------------------------
	// Static Fields
	// ----------------------------------------------------------
	
	/*
	 * The serialVersionUID of this 
	 */
	private static final long serialVersionUID = 4439817275715274766L;
	
	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	/*
	 * The related Media object
	 */
	private Media media;

	/*
	 * Represents the barcode which identifies the Book
	 */
	private String code;

	/*
	 * Reference to the Account who has borrowed this entry, null if this entry is available
	 */
	private Account target;

	/*
	 * Holds all supplement entries of this Book
	 */
	private Set<MediaEntry> supplements = new HashSet<>();

	/*
	 * Keeps track of all actions performed
	 */
	private List<MediaAction> actions = new ArrayList<>();

	/*
	 * The condition of this MediaEntry
	 */
	private MediaCondition condition;

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------

	/**
	 * Creates a new MediaEntry.
	 * 
	 * @param media
	 */
	public MediaEntry(Media media) {
		checkMedia(media);
		this.media = media;
	}
	
	/**
	 * Creates a new MediaEntry.
	 * 
	 * @param media
	 */
	public MediaEntry(Media media, String code) {
		setMedia(media);
		setCode(code);
	}

	// ----------------------------------------------------------
	// Validation
	// ----------------------------------------------------------

	/**
	 * Validates a specific Media and throws an exception if the Object is invalid.
	 * 
	 * @param media the media
	 */
	private void checkMedia(Media media) {
		Objects.requireNonNull(media, "Media provided cannot be null.");
	}

	/**
	 * Validates a specific code and throws an exception if it is invalid.
	 * 
	 * @param code the code to validate
	 */
	private void checkCode(String code) {
		if (code.isEmpty()) {
			throw new IllegalArgumentException("The provided code is invalid.");
		}
	}

	// ----------------------------------------------------------
	// Methods
	// ----------------------------------------------------------

	// ----------------------------------------------------------
	// Accessors
	// ----------------------------------------------------------

	/**
	 * Returns the associated Media that this entry belongs to.
	 * 
	 * @return the associated
	 */
	public Media getMedia() {
		return media;
	}

	/**
	 * Sets the associated Media that this entry belongs to.
	 * 
	 * @param media the associated Media
	 */
	public void setMedia(Media media) {
		checkMedia(media);
		this.media = media;
	}

	/**
	 * Returns the code that uniquely identifies this entry, which may also be known as the
	 * "barcode".
	 * 
	 * @return the code the barcode that identifies this Book
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code that uniquely identifies this entry.
	 * 
	 * @param code the code that identifies this Book, not null or empty
	 */
	public void setCode(String code) {
		checkCode(code);
		this.code = code;
	}

	/**
	 * Returns the target account who has borrowed the media if existing, otherwise null will be
	 * returned.
	 * 
	 * @return the account who borrowed the media, otherwise null
	 */
	public Account getTarget() {
		return target;
	}

	/**
	 * Sets the target account who has borrowed the media and overrides it, if this media is already
	 * borrowed.
	 * 
	 * @param target the target account who has borrowed
	 */
	public void setTarget(Account target) {
		this.target = target;
	}

	/**
	 * Returns the supplements.
	 * 
	 * @return the supplements
	 */
	public Set<MediaEntry> getSupplements() {
		return supplements;
	}

	/**
	 * Sets the supplements.
	 * 
	 * @param supplements the supplements to set
	 */
	public void setSupplements(Set<MediaEntry> supplements) {
		this.supplements = supplements;
	}

	/**
	 * Adds a new MediaAction to this MediaEntry.
	 * 
	 * @param action the action to add
	 */
	public void addAction(MediaAction action) {
		this.actions.add(action);
	}

	/**
	 * Returns all actions that have been performed on this media.
	 * 
	 * @return the actions performed on this media
	 */
	public List<MediaAction> getActions() {
		return actions;
	}

	/**
	 * Returns the condition of this media.
	 * 
	 * @return the condition of this media
	 */
	public MediaCondition getCondition() {
		return condition;
	}

	/**
	 * Sets the condition of this media.
	 * 
	 * @param condition the condition of this media to set
	 */
	public void setCondition(MediaCondition condition) {
		this.condition = condition;
	}
	
	// ----------------------------------------------------------
	// Other
	// ----------------------------------------------------------
	
	@Override
	public String toString() {
		final int maxLen = 5;
		StringBuilder builder = new StringBuilder();
		builder.append("MediaEntry [media=");
		builder.append(media);
		builder.append(", code=");
		builder.append(code);
		builder.append(", target=");
		builder.append(target);
		builder.append(", supplements=");
		builder.append(supplements != null ? toString(supplements, maxLen) : null);
		builder.append(", actions=");
		builder.append(actions != null ? toString(actions, maxLen) : null);
		builder.append(", condition=");
		builder.append(condition);
		builder.append("]");
		return builder.toString();
	}

	private String toString(Collection<?> collection, int maxLen) {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		int i = 0;
		for (Iterator<?> iterator = collection.iterator(); iterator.hasNext() && i < maxLen; i++) {
			if (i > 0) builder.append(", ");
			builder.append(iterator.next());
		}
		builder.append("]");
		return builder.toString();
	}

}
