
/*
 * (C) Copyright 2016 Lukas Kannenberg
 */

/**
 * 
 */

package obook.source.model;

import java.io.Serializable;

/**
 * // TODO insert type description here
 *
 * @author Lukas
 * @since 2016-07-15
 * @version 0.0.1
 *
 */
public class MediaImageInfo implements Serializable {

	// ----------------------------------------------------------
	// Static Fields
	// ----------------------------------------------------------

	/**
	 *  The serial Version UID of this class
	 */
	private static final long serialVersionUID = 5658349632894226124L;

	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	/*
	 * The online reference link to a small thumbnail of the related item
	 */
	private String smallThumbnailLink;

	/*
	 * The online reference link to a small thumbnail of the related item
	 */
	private String thumbnailLink;

	/*
	 * The online reference link to a small thumbnail of the related item
	 */
	private String smallLink;

	/*
	 * The online reference link to a small thumbnail of the related item
	 */
	private String mediumLink;

	/*
	 * The online reference link to a small thumbnail of the related item
	 */
	private String largeLink;

	/*
	 * The online reference link to a small thumbnail of the related item
	 */
	private String extraLargeLink;

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------

	/**
	 * TODO
	 *
	 * @param smallThumbnailLink
	 * @param thumbnailLink
	 * @param smallLink
	 * @param mediumLink
	 * @param largeLink
	 * @param extraLargeLink
	 */
	public MediaImageInfo(String smallThumbnailLink, String thumbnailLink, String smallLink, String mediumLink,
			String largeLink, String extraLargeLink) {
		this.smallThumbnailLink = smallThumbnailLink;
		this.thumbnailLink = thumbnailLink;
		this.smallLink = smallLink;
		this.mediumLink = mediumLink;
		this.largeLink = largeLink;
		this.extraLargeLink = extraLargeLink;
	}

	// ----------------------------------------------------------
	// Accessors
	// ----------------------------------------------------------

	/**
	 * Returns the smallThumbnailLink.
	 * 
	 * @return the smallThumbnailLink
	 */
	public String getSmallThumbnailLink() {
		return smallThumbnailLink;
	}

	/**
	 * Sets the smallThumbnailLink.
	 * 
	 * @param smallThumbnailLink the smallThumbnailLink to set
	 */
	public void setSmallThumbnailLink(String smallThumbnailLink) {
		this.smallThumbnailLink = smallThumbnailLink;
	}

	/**
	 * Returns the thumbnailLink.
	 * 
	 * @return the thumbnailLink
	 */
	public String getThumbnailLink() {
		return thumbnailLink;
	}

	/**
	 * Sets the thumbnailLink.
	 * 
	 * @param thumbnailLink the thumbnailLink to set
	 */
	public void setThumbnailLink(String thumbnailLink) {
		this.thumbnailLink = thumbnailLink;
	}

	/**
	 * Returns the smallLink.
	 * 
	 * @return the smallLink
	 */
	public String getSmallLink() {
		return smallLink;
	}

	/**
	 * Sets the smallLink.
	 * 
	 * @param smallLink the smallLink to set
	 */
	public void setSmallLink(String smallLink) {
		this.smallLink = smallLink;
	}

	/**
	 * Returns the mediumLink.
	 * 
	 * @return the mediumLink
	 */
	public String getMediumLink() {
		return mediumLink;
	}

	/**
	 * Sets the mediumLink.
	 * 
	 * @param mediumLink the mediumLink to set
	 */
	public void setMediumLink(String mediumLink) {
		this.mediumLink = mediumLink;
	}

	/**
	 * Returns the largeLink.
	 * 
	 * @return the largeLink
	 */
	public String getLargeLink() {
		return largeLink;
	}

	/**
	 * Sets the largeLink.
	 * 
	 * @param largeLink the largeLink to set
	 */
	public void setLargeLink(String largeLink) {
		this.largeLink = largeLink;
	}

	/**
	 * Returns the extraLargeLink.
	 * 
	 * @return the extraLargeLink
	 */
	public String getExtraLargeLink() {
		return extraLargeLink;
	}

	/**
	 * Sets the extraLargeLink.
	 * 
	 * @param extraLargeLink the extraLargeLink to set
	 */
	public void setExtraLargeLink(String extraLargeLink) {
		this.extraLargeLink = extraLargeLink;
	}
	
	// ----------------------------------------------------------
	// Other
	// ----------------------------------------------------------

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ImageInformation [smallThumbnailLink=");
		builder.append(smallThumbnailLink);
		builder.append(", thumbnailLink=");
		builder.append(thumbnailLink);
		builder.append(", smallLink=");
		builder.append(smallLink);
		builder.append(", mediumLink=");
		builder.append(mediumLink);
		builder.append(", largeLink=");
		builder.append(largeLink);
		builder.append(", extraLargeLink=");
		builder.append(extraLargeLink);
		builder.append("]");
		return builder.toString();
	}
	
}
