
/*
 * (C) Copyright 2016 Lukas Kannenberg
 */

/**
 * 
 */

package obook.source.model;

import java.io.Serializable;

/**
 * Represents price information for a related Media object.
 *
 * @author Lukas Kannenberg
 * @since 2016-07-15
 * @version 1.0.1
 *
 */
public class MediaPriceInfo implements Serializable {

	// ----------------------------------------------------------
	// Static Fields
	// ----------------------------------------------------------
	
	/**
	 *  The serial Version UID of this class
	 */
	private static final long serialVersionUID = -1740914816977310299L;
	
	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	/*
	 * The country code of the related item
	 */
	private String countryCode;

	/*
	 * Whether or not related item is salable
	 */
	private boolean salable;

	/*
	 * The retail price of the related item
	 */
	private double retailPrice;

	/*
	 * The currency code for the retail price of the related item
	 */
	private String retailCurrencyCode;

	/*
	 * The online reference link of the related item
	 */
	private String link;

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------

	/**
	 * Constructs related price information for an existing Media object.
	 *
	 * @param countryCode the country code of the related item
	 * @param salable whether or not this item is salable
	 * @param retailPrice the retail price of the related item
	 * @param retailCurrencyCode currency code for the retail price of the related item
	 * @param link the online reference link of the related item
	 */
	public MediaPriceInfo(String countryCode, boolean salable, double retailPrice, String retailCurrencyCode,
			String link) {
		this.countryCode = countryCode;
		this.salable = salable;
		this.retailPrice = retailPrice;
		this.retailCurrencyCode = retailCurrencyCode;
		this.link = link;
	}

	// ----------------------------------------------------------
	// Accessors
	// ----------------------------------------------------------

	/**
	 * Returns the countryCode.
	 * 
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * Sets the countryCode.
	 * 
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * Returns the salable.
	 * 
	 * @return the salable
	 */
	public boolean isSalable() {
		return salable;
	}

	/**
	 * Sets the salable.
	 * 
	 * @param salable the salable to set
	 */
	public void setSalable(boolean salable) {
		this.salable = salable;
	}

	/**
	 * Returns the retailPrice.
	 * 
	 * @return the retailPrice
	 */
	public double getRetailPrice() {
		return retailPrice;
	}

	/**
	 * Sets the retailPrice.
	 * 
	 * @param retailPrice the retailPrice to set
	 */
	public void setRetailPrice(double retailPrice) {
		this.retailPrice = retailPrice;
	}

	/**
	 * Returns the retailCurrencyCode.
	 * 
	 * @return the retailCurrencyCode
	 */
	public String getRetailCurrencyCode() {
		return retailCurrencyCode;
	}

	/**
	 * Sets the retailCurrencyCode.
	 * 
	 * @param retailCurrencyCode the retailCurrencyCode to set
	 */
	public void setRetailCurrencyCode(String retailCurrencyCode) {
		this.retailCurrencyCode = retailCurrencyCode;
	}

	/**
	 * Returns the link.
	 * 
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * Sets the link.
	 * 
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}
	
	// ----------------------------------------------------------
	// Other
	// ----------------------------------------------------------

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PriceInformation [countryCode=");
		builder.append(countryCode);
		builder.append(", salable=");
		builder.append(salable);
		builder.append(", retailPrice=");
		builder.append(retailPrice);
		builder.append(", retailCurrencyCode=");
		builder.append(retailCurrencyCode);
		builder.append(", link=");
		builder.append(link);
		builder.append("]");
		return builder.toString();
	}
	
}
