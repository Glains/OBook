/*
 * (C) Copyright 2016 Lukas Kannenberg
 */
package obook.source.model;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Represents a classic book.
 * 
 * @author Lukas Kannenberg
 * @since 2016-03-13
 * @version 1.0.0
 *
 */
public class Book extends Media {

	// ----------------------------------------------------------
	// Static Fields
	// ----------------------------------------------------------

	/**
	 * The serial Version UID of this class
	 */
	private static final long serialVersionUID = -6297713733531100016L;

	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	/*
	 * The title of this Book
	 */
	private String title;

	/*
	 * The subtitle of this Book
	 */
	private String subTitle;

	/*
	 * The authors of this Book
	 */
	private Set<String> authors = new HashSet<>();

	/*
	 * The publisher of this Book
	 */
	private String publisher;

	/*
	 * The date this Book has been published
	 */
	private Date published;

	/*
	 * The description of this Book
	 */
	private String description;

	/*
	 * The industrial identifies of this Book
	 */
	private Set<IndustrialIdentifier> identifiers = new HashSet<>();

	/*
	 * The amount of pages of this Book
	 */
	private int pageCount;

	/*
	 * The main Category of this Book
	 */
	private MediaCategory mainCategory;

	/*
	 * The language code of the language that this Book is written in
	 */
	private String languageCode;

	/*
	 * The preview link of this Book
	 */
	private String previewLink;

	/*
	 * The info link of this Book
	 */
	private String infoLink;
	
	/*
	 * Price information
	 */
	private MediaPriceInfo priceInfo;

	/*
	 * The ImageInformation of this Book
	 */
	private MediaImageInfo imageInfo;

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------

	/**
	 * TODO
	 *
	 * @param title
	 * @param subTitle
	 * @param description
	 */
	public Book(String id, String title, String subTitle, String description) {
		super(id);
		this.title = title;
		this.subTitle = subTitle;
		this.description = description;
	}

	// ----------------------------------------------------------
	// Accessors
	// ----------------------------------------------------------

	/**
	 * Returns the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Returns the subTitle.
	 * 
	 * @return the subTitle
	 */
	public String getSubTitle() {
		return subTitle;
	}

	/**
	 * Sets the subTitle.
	 * 
	 * @param subTitle the subTitle to set
	 */
	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	/**
	 * Returns the authors.
	 * 
	 * @return the authors
	 */
	public Set<String> getAuthors() {
		return authors;
	}

	/**
	 * Sets the authors.
	 * 
	 * @param authors the authors to set
	 */
	public void setAuthors(Set<String> authors) {
		this.authors = authors;
	}

	/**
	 * Returns the publisher.
	 * 
	 * @return the publisher
	 */
	public String getPublisher() {
		return publisher;
	}

	/**
	 * Sets the publisher.
	 * 
	 * @param publisher the publisher to set
	 */
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	/**
	 * Returns the published.
	 * 
	 * @return the published
	 */
	public Date getPublished() {
		return published;
	}

	/**
	 * Sets the published.
	 * 
	 * @param published the published to set
	 */
	public void setPublished(Date published) {
		this.published = published;
	}

	/**
	 * Returns the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the identifiers.
	 * 
	 * @return the identifiers
	 */
	public Set<IndustrialIdentifier> getIdentifiers() {
		return identifiers;
	}

	/**
	 * Sets the identifiers.
	 * 
	 * @param identifiers the identifiers to set
	 */
	public void setIdentifiers(Set<IndustrialIdentifier> identifiers) {
		this.identifiers = identifiers;
	}

	/**
	 * Returns the pageCount.
	 * 
	 * @return the pageCount
	 */
	public int getPageCount() {
		return pageCount;
	}

	/**
	 * Sets the pageCount.
	 * 
	 * @param pageCount the pageCount to set
	 */
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	/**
	 * Returns the mainCategory.
	 * 
	 * @return the mainCategory
	 */
	public MediaCategory getMainCategory() {
		return mainCategory;
	}

	/**
	 * Sets the mainCategory.
	 * 
	 * @param mainCategory the mainCategory to set
	 */
	public void setMainCategory(MediaCategory mainCategory) {
		this.mainCategory = mainCategory;
	}

	/**
	 * Returns the languageCode.
	 * 
	 * @return the languageCode
	 */
	public String getLanguageCode() {
		return languageCode;
	}

	/**
	 * Sets the languageCode.
	 * 
	 * @param languageCode the languageCode to set
	 */
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * Returns the previewLink.
	 * 
	 * @return the previewLink
	 */
	public String getPreviewLink() {
		return previewLink;
	}

	/**
	 * Sets the previewLink.
	 * 
	 * @param previewLink the previewLink to set
	 */
	public void setPreviewLink(String previewLink) {
		this.previewLink = previewLink;
	}

	/**
	 * Returns the infoLink.
	 * 
	 * @return the infoLink
	 */
	public String getInfoLink() {
		return infoLink;
	}

	/**
	 * Sets the infoLink.
	 * 
	 * @param infoLink the infoLink to set
	 */
	public void setInfoLink(String infoLink) {
		this.infoLink = infoLink;
	}
	
	/**
	 * Returns the price of this media.
	 * 
	 * @return the price
	 */
	public MediaPriceInfo getPriceInformation() {
		return priceInfo;
	}

	/**
	 * Sets the price of this media.
	 * 
	 * @param price the price to set
	 */
	public void setPriceInformation(MediaPriceInfo price) {
		this.priceInfo = price;
	}

	/**
	 * Returns the image code of this media.
	 * 
	 * @return the image code of this media
	 */
	public MediaImageInfo getImageCode() {
		return imageInfo;
	}

	/**
	 * Sets the image code of this media.
	 * 
	 * @param imageCode the image code to set
	 */
	public void setImageCode(MediaImageInfo imageInfo) {
		this.imageInfo = imageInfo;
	}
	
	// ----------------------------------------------------------
	// Other
	// ----------------------------------------------------------
	
	@Override
	public String toString() {
		final int maxLen = 5;
		StringBuilder builder = new StringBuilder();
		builder.append("Book [id=");
		builder.append(id);
		builder.append(", title=");
		builder.append(title);
		builder.append(", subTitle=");
		builder.append(subTitle);
		builder.append(", categories=");
		builder.append(categories != null ? toString(categories, maxLen) : null);
		builder.append(", authors=");
		builder.append(authors != null ? toString(authors, maxLen) : null);
		builder.append(", publisher=");
		builder.append(publisher);
		builder.append(", published=");
		builder.append(published);
		builder.append("]");
		return builder.toString();
	}

	private String toString(Collection<?> collection, int maxLen) {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		int i = 0;
		for (Iterator<?> iterator = collection.iterator(); iterator.hasNext() && i < maxLen; i++) {
			if (i > 0) builder.append(", ");
			builder.append(iterator.next());
		}
		builder.append("]");
		return builder.toString();
	}
	
}
