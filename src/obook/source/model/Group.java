package obook.source.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import obook.source.target.account.Account;

public class Group implements Serializable {

	// ----------------------------------------------------------
	// Static Fields
	// ----------------------------------------------------------

	/**
	 * The serialVersionUID of this
	 */
	private static final long serialVersionUID = -273635906420764445L;

	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	/**
	 * The name of this Group.
	 */
	private String name;

	/**
	 * The description of this Group.
	 */
	private String description;

	/**
	 * The parent Group of this Group
	 */
	private Group parent;

	/**
	 * The supplement Groups of this Category
	 */
	private Set<Group> subGroups = new HashSet<>();

	/**
	 * A Set of accessible categories of this Group
	 * 
	 * @deprecated functionality will be removed with the next update, each
	 *             Group will have a Set of Media instead of using a Set of
	 *             MediaCategory
	 */
	@Deprecated
	private Set<MediaCategory> categories = new HashSet<>();

	/**
	 * Whitelist of Categories that should be inherited form the parent Category
	 */
	private Set<MediaCategory> whiteList = new HashSet<>();

	/**
	 * Blacklist of Categories that should not be inherited form the parent
	 * Category
	 */
	private Set<MediaCategory> blackList = new HashSet<>();

	/**
	 * A Set holding all Accounts which belong to this Group
	 */
	private Set<Media> media = new HashSet<>();

	/**
	 * A Set holding all Accounts which belong to this Group
	 */
	private Set<Account> members = new HashSet<>();

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------

	/**
	 * 
	 * TODO
	 *
	 * @param name
	 * @param description
	 */
	public Group(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	// ----------------------------------------------------------
	// Accessors
	// ----------------------------------------------------------

	/**
	 * Returns the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the parent.
	 * 
	 * @return the parent
	 */
	public Group getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 * 
	 * @param parent
	 *            the parent to set
	 */
	public void setParent(Group parent) {
		this.parent = parent;
	}

	/**
	 * Returns the subGroups.
	 * 
	 * @return the subGroups
	 */
	public Set<Group> getSubGroups() {
		return subGroups;
	}

	/**
	 * Sets the subGroups.
	 * 
	 * @param subGroups
	 *            the subGroups to set
	 */
	public void setSubGroups(Set<Group> subGroups) {
		this.subGroups = subGroups;
	}

	/**
	 * Returns the categories.
	 * 
	 * @return the categories
	 * @deprecated functionality will be removed with the next update, each
	 *             Group will have a Set of Media instead of using a Set of
	 *             MediaCategory
	 */
	@Deprecated
	public Set<MediaCategory> getCategories() {
		return categories;
	}

	/**
	 * Sets the categories.
	 * 
	 * @param categories
	 *            the categories to set
	 *            @deprecated functionality will be removed with the next update, each
	 *             Group will have a Set of Media instead of using a Set of
	 *             MediaCategory
	 */
	@Deprecated
	public void setCategories(Set<MediaCategory> categories) {
		this.categories = categories;
	}

	/**
	 * Returns the whiteList.
	 * 
	 * @return the whiteList
	 */
	public Set<MediaCategory> getWhiteList() {
		return whiteList;
	}

	/**
	 * Sets the whiteList.
	 * 
	 * @param whiteList
	 *            the whiteList to set
	 */
	public void setWhiteList(Set<MediaCategory> whiteList) {
		this.whiteList = whiteList;
	}

	/**
	 * Returns the blackList.
	 * 
	 * @return the blackList
	 */
	public Set<MediaCategory> getBlackList() {
		return blackList;
	}

	/**
	 * Sets the blackList.
	 * 
	 * @param blackList
	 *            the blackList to set
	 */
	public void setBlackList(Set<MediaCategory> blackList) {
		this.blackList = blackList;
	}

	/**
	 * Returns the media.
	 * 
	 * @return the media
	 */
	public Set<Media> getMedia() {
		return media;
	}

	/**
	 * Sets the media.
	 * 
	 * @param media
	 *            the media to set
	 */
	public void setMedia(Set<Media> media) {
		this.media = media;
	}

	/**
	 * Returns the members.
	 * 
	 * @return the members
	 */
	public Set<Account> getMembers() {
		return members;
	}

	/**
	 * Sets the members.
	 * 
	 * @param members
	 *            the members to set
	 */
	public void setMembers(Set<Account> members) {
		this.members = members;
	}

	// ----------------------------------------------------------
	// Other
	// ----------------------------------------------------------

	@Override
	public String toString() {
		final int maxLen = 5;
		StringBuilder builder = new StringBuilder();
		builder.append("Group [name=");
		builder.append(name);
		builder.append(", description=");
		builder.append(description);
		builder.append(", parent=");
		builder.append(parent);
		builder.append(", subGroups=");
		builder.append(subGroups != null ? toString(subGroups, maxLen) : null);
		builder.append(", categories=");
		builder.append(categories != null ? toString(categories, maxLen) : null);
		builder.append(", members=");
		builder.append(members != null ? toString(members, maxLen) : null);
		builder.append("]");
		return builder.toString();
	}

	private String toString(Collection<?> collection, int maxLen) {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		int i = 0;
		for (Iterator<?> iterator = collection.iterator(); iterator.hasNext() && i < maxLen; i++) {
			if (i > 0)
				builder.append(", ");
			builder.append(iterator.next());
		}
		builder.append("]");
		return builder.toString();
	}

}
