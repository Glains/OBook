
/*
 * (C) Copyright 2016 Lukas Kannenberg
 */

/**
 * 
 */
package obook.source.model;

import java.io.Serializable;

/**
 * // TODO insert type description here
 *
 * @author Lukas
 * @since 2016-07-17
 * @version 0.0.1
 *
 */
public class MediaCondition implements Serializable {

	// ----------------------------------------------------------
	// Static Fields
	// ----------------------------------------------------------

	/**
	 * The serialVersionUID of this
	 */
	private static final long serialVersionUID = 8197946095591336811L;
	
	public static MediaCondition CONDITION_OK = new MediaCondition("OK", "The Media is in a good condition.");
	
	public static MediaCondition CONDITION_DAMAGED = new MediaCondition("DAMAGED", "The Media has been damaged.");
	
	public static MediaCondition CONDITION_UNUSABLE = new MediaCondition("UNUSABLE", "The Media cannot be used anymore.");

	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	/*
	 * The title of this MediaCondition
	 */
	private String title;

	/*
	 * The description of this MediaCondition
	 */
	private String description;

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------

	/**
	 * TODO
	 *
	 * @param title
	 * @param description
	 */
	public MediaCondition(String title, String description) {
		super();
		this.title = title;
		this.description = description;
	}

	// ----------------------------------------------------------
	// Accessors
	// ----------------------------------------------------------

	/**
	 * Returns the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Returns the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	// ----------------------------------------------------------
	// Other
	// ----------------------------------------------------------

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MediaCondition [title=");
		builder.append(title);
		builder.append(", description=");
		builder.append(description);
		builder.append("]");
		return builder.toString();
	}

}
