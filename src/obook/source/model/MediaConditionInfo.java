/*
 * (C) Copyright 2016 Lukas Kannenberg
 */
package obook.source.model;

import java.io.Serializable;

/**
 * 
 * // TODO insert type description here
 *
 * @author Lukas Kannenberg
 * @since 2016-07-17
 * @version 0.0.1
 *
 */
public class MediaConditionInfo implements Serializable {

	// ----------------------------------------------------------
	// Static Fields
	// ----------------------------------------------------------

	/**
	 * The serialVersionUID of this
	 */
	private static final long serialVersionUID = 4357677098241229063L;

	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	/*
	 * The state for this media
	 */
	private MediaCondition state;

	/*
	 * The additional description
	 */
	private String description;

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------

	/**
	 * Constructs new Media information containing a state.
	 * 
	 * @param state the state
	 */
	public MediaConditionInfo(MediaCondition state) {
		setState(state);
	}

	/**
	 * Constructs new Media information containing a state and an additional description.
	 * 
	 * @param state the state
	 * @param desc the description
	 */
	public MediaConditionInfo(MediaCondition state, String desc) {
		setState(state);
		setDescription(desc);
	}

	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	/**
	 * Returns the {@link MediaCondition}.
	 * 
	 * @return the state
	 */
	public MediaCondition getState() {
		return state;
	}

	/**
	 * Sets the {@link MediaCondition}
	 * 
	 * @param state the state to set
	 */
	public void setState(MediaCondition state) {
		this.state = state;
	}

	/**
	 * Returns the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	// ----------------------------------------------------------
	// Other
	// ----------------------------------------------------------

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MediaConditionInfo [state=");
		builder.append(state);
		builder.append(", description=");
		builder.append(description);
		builder.append("]");
		return builder.toString();
	}

}
