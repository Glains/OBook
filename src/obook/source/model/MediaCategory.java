
/*
 * (C) Copyright 2016 Lukas Kannenberg
 */

package obook.source.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * 
 * The Category serves as a virtual container to group certain Media objects.
 * Adding the same Category to similar Media objects provides more clarity and
 * transparency.
 *
 * @author Lukas Kannenberg
 * @since 2016-07-15
 * @version 1.0.1
 *
 */
public class MediaCategory implements Serializable {

	// ----------------------------------------------------------
	// Static Fields
	// ----------------------------------------------------------

	/**
	 * The serial Version UID of this class
	 */
	private static final long serialVersionUID = 704111227324845725L;

	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	/*
	 * The name of this Category
	 */
	private String name;

	/*
	 * The parent Category of this Category
	 */
	private MediaCategory parentCategory;

	/*
	 * The supplement Categories of this Category
	 */
	private Set<MediaCategory> subCategories = new HashSet<>();

	/*
	 * A Set holding all included Media
	 */
	private Set<Media> members = new HashSet<>();

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------

	/**
	 * Constructs a new Category containing a name.
	 *
	 * @param name
	 *            the name of this Category
	 */
	public MediaCategory(String name) {
		this.name = name;
	}

	/**
	 * 
	 * Constructs a new Category containing a name and a parent Category.
	 *
	 * @param name
	 *            name the name of this Category
	 * @param parent
	 *            name the name of the parent Category
	 */
	public MediaCategory(String name, MediaCategory parent) {
		this.name = name;
		this.parentCategory = parent;
	}

	// ----------------------------------------------------------
	// Accessors
	// ----------------------------------------------------------

	/**
	 * Returns the name of this Category.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of this Category.
	 * 
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the parentCategory.
	 * 
	 * @return the parentCategory
	 */
	public MediaCategory getParentCategory() {
		return parentCategory;
	}

	/**
	 * Sets the parentCategory.
	 * 
	 * @param parentCategory
	 *            the parentCategory to set
	 */
	public void setParentCategory(MediaCategory parentCategory) {
		this.parentCategory = parentCategory;
	}

	/**
	 * Returns the subordinate Categories of this Category.
	 * 
	 * @return the subordinate Categories
	 */
	public Set<MediaCategory> getSubCategories() {
		return subCategories;
	}

	/**
	 * Sets the subordinate Categories of this Category.
	 * 
	 * @param subCategories
	 *            the subordinate Categories to set
	 */
	public void setSubCategories(Set<MediaCategory> subCategories) {
		this.subCategories = subCategories;
	}

	/**
	 * Returns the members.
	 * 
	 * @return the members
	 */
	public Set<Media> getMembers() {
		return members;
	}

	/**
	 * Sets the members.
	 * 
	 * @param members
	 *            the members to set
	 */
	public void setMembers(Set<Media> members) {
		this.members = members;
	}

	// ----------------------------------------------------------
	// Other
	// ----------------------------------------------------------

	@Override
	public String toString() {
		final int maxLen = 5;
		StringBuilder builder = new StringBuilder();
		builder.append("Category [name=");
		builder.append(name);
		builder.append(", parentCategory=");
		builder.append(parentCategory != null ? parentCategory.getName() : null);
		builder.append(", subCategories=");
		builder.append(subCategories != null ? toString(subCategories, maxLen) : null);
		builder.append(", membersSize=");
		builder.append(members != null ? members.size() : null);
		builder.append("]");
		return builder.toString();
	}

	private String toString(Collection<?> collection, int maxLen) {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		int i = 0;
		for (Iterator<?> iterator = collection.iterator(); iterator.hasNext() && i < maxLen; i++) {
			if (i > 0)
				builder.append(", ");
			builder.append(iterator.next());
		}
		builder.append("]");
		return builder.toString();
	}

}
