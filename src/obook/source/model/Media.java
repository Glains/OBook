
/*
 * (C) Copyright 2016 Lukas Kannenberg
 */

package obook.source.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * The Media class is an abstract class that represents any Media. This class may be extended to
 * implement specific media types. Each Media consists of an unique id which identifies it. Media
 * may have multiple supplemented Media.
 * 
 * @author Lukas Kannenberg
 * @since 2016-03-13
 * @version 1.0.3
 *
 */
public abstract class Media implements Serializable {

	// ----------------------------------------------------------
	// Static Fields
	// ----------------------------------------------------------

	/**
	 * The serial Version UID of this class
	 */
	private static final long serialVersionUID = -1030351082036827821L;

	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	/**
	 * The unique id of this Media
	 */
	protected String id;

	/**
	 * The MediaCategories for this Media
	 */
	protected Set<MediaCategory> categories = new HashSet<>();
	
	/**
	 * The Groups for this Media
	 */
	protected Set<Group> groups = new HashSet<>();

	/**
	 * The supplements of this Media
	 */
	protected Set<Media> supplements = new HashSet<>();

	/**
	 * A list of MediaEntries of this Media
	 */
	protected Set<MediaEntry> entries;

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------

	/**
	 * Constructs a new Media with a given id.
	 * 
	 * @param id the id of the Media
	 */
	protected Media(String id) {
		setID(id);
	}

	// ----------------------------------------------------------
	// Accessors
	// ----------------------------------------------------------

	/**
	 * Returns the id of this media.
	 * 
	 * @return the id for of this media
	 */
	public String getID() {
		return id;
	}

	/**
	 * Sets the name of this media.
	 * 
	 * @param id the id to set
	 */
	public void setID(String id) {
		this.id = id;
	}

	/**
	 * Returns the categories.
	 * 
	 * @return the categories
	 */
	public Set<MediaCategory> getCategories() {
		return categories;
	}

	/**
	 * Sets the categories.
	 * 
	 * @param categories the categories to set
	 */
	public void setCategories(Set<MediaCategory> categories) {
		this.categories = categories;
	}

	/**
	 * Returns the groups.
	 * 
	 * @return the groups
	 */
	public Set<Group> getGroups() {
		return groups;
	}

	/**
	 * Sets the groups.
	 * 
	 * @param groups the groups to set
	 */
	public void setGroups(Set<Group> groups) {
		this.groups = groups;
	}

	/**
	 * Returns the supplements.
	 * 
	 * @return the supplements
	 */
	public Set<Media> getSupplements() {
		return supplements;
	}

	/**
	 * Sets the supplements.
	 * 
	 * @param supplements the supplements to set
	 */
	public void setSupplements(Set<Media> supplements) {
		this.supplements = supplements;
	}

	/**
	 * Returns the entries.
	 * 
	 * @return the entries
	 */
	public Set<MediaEntry> getEntries() {
		return entries;
	}

	/**
	 * Sets the entries.
	 * 
	 * @param entries the entries to set
	 */
	public void setEntries(Set<MediaEntry> entries) {
		this.entries = entries;
	}

	// ----------------------------------------------------------
	// Other
	// ----------------------------------------------------------

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Media [id=");
		builder.append(id);
		builder.append(", supplements=");
		builder.append(supplements);
		builder.append(", entries=");
		builder.append(entries);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (!(obj instanceof Media)) return false;
		Media other = (Media) obj;
		if (id == null) {
			if (other.id != null) return false;
		} else if (!id.equals(other.id)) return false;
		return true;
	}

}
