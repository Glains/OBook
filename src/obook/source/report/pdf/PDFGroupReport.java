package obook.source.report.pdf;

import obook.source.report.PDFReportGenerator;
import obook.source.report.Report;

public class PDFGroupReport implements PDFReportGenerator {

	@Override
	public String generatePDFReport(Report report) {
		return "Generated PDF Group Report";
	}

}
