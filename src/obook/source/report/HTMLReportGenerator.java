package obook.source.report;

import j2html.tags.Tag;

public interface HTMLReportGenerator {

	public Tag generateHTMLReport(Report report);

}
