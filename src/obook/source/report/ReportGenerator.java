package obook.source.report;

public class ReportGenerator {

	public static ReportFactory getReportfactory(ReportType type) {
		if (type == ReportType.PDF) {
			return new PDFReportFactory();
		}
		if (type == ReportType.HTML) {
			return new HTMLReportFactory();
		}
		return null;
	}
}
