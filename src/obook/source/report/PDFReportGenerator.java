package obook.source.report;

public interface PDFReportGenerator {

	public String generatePDFReport(Report report);

}
