package obook.source.report;

import j2html.tags.Tag;
import obook.source.report.pdf.PDFGroupReport;
import obook.tosort.GroupReport;

class PDFReportFactory extends ReportFactory {

	@Override
	public String generatePDFReport(Report report) {
		if (report instanceof GroupReport) {
			return new PDFGroupReport().generatePDFReport(report);
		} else {
			throw new UnsupportedOperationException("This Report does not support PDF Report Generation.");
		}
	}

	@Override
	public Tag generateHTMLReport(Report report) {
		return null;
	}

}
