package obook.source.report;

public enum ReportType {

	HTML, PDF;
}
