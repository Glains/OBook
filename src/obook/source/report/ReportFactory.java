package obook.source.report;

import j2html.tags.Tag;

public abstract class ReportFactory {

	public abstract String generatePDFReport(Report report);
	
	public abstract Tag generateHTMLReport(Report report);
}
