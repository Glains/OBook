package obook.source.report;

import j2html.tags.Tag;
import obook.source.report.html.HTMLGroupReport;
import obook.tosort.GroupReport;

class HTMLReportFactory extends ReportFactory {

	@Override
	public String generatePDFReport(Report report) {
		return null;
	}

	@Override
	public Tag generateHTMLReport(Report report) {
		if (report instanceof GroupReport) {
			return new HTMLGroupReport().generateHTMLReport(report);
		} else {
			throw new UnsupportedOperationException("This Report does not support HTML Report Generation.");
		}
	}

}
