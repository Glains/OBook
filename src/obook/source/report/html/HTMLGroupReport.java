package obook.source.report.html;

import static j2html.TagCreator.body;
import static j2html.TagCreator.h1;
import static j2html.TagCreator.head;
import static j2html.TagCreator.html;
import static j2html.TagCreator.link;
import static j2html.TagCreator.main;
import static j2html.TagCreator.title;

import j2html.tags.Tag;
import obook.source.report.HTMLReportGenerator;
import obook.source.report.Report;

public class HTMLGroupReport implements HTMLReportGenerator {

	@Override
	public Tag generateHTMLReport(Report report) {
		return html().with(head().with(title("Title"), link().withRel("stylesheet").withHref("/css/main.css")),
				body().with(main().with(h1("Heading!"))));
	}

}
