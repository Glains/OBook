package obook.source.manager;

import java.util.Set;

import obook.source.model.Book;
import obook.source.model.Group;
import obook.source.model.Media;
import obook.source.model.MediaCategory;
import obook.source.model.MediaEntry;

public interface MediaManager {

	public int register(Media media);

	public int register(MediaEntry entry);

	public void update(Media media);

	public void update(MediaEntry entry);

	public void delete(Media media);

	public void delete(MediaEntry entry);

	// ----------------------------------------------------------
	// Media Queries
	// ----------------------------------------------------------

	public Set<Media> getMediaByName(String name);

	public Set<Media> getMediaByCategory(String category);

	// ----------------------------------------------------------
	// Book Queries
	// ----------------------------------------------------------

	public Set<Book> getBooksByTitle(String title);

	public Set<Book> getBooksByAuthor(String author);
	
	/**
	 * 
	 * Returns a <code>Set</code> of all accessible media categories for this
	 * group. This includes all media categories from the group itself and all
	 * other media categories from parent groups. Also, any white- or blacklist
	 * filters will be applied during the process.
	 * <p>
	 * 
	 * To obtain a <code>Set</code> of all accessible media for a specific
	 * group, simple iterate through all accessible media categories: <br>
	 * 
	 * <pre>
	 * <code>
	 * Set&ltMedia&gt media = new HashSet<>();
	 * for(MediaCategory c : {@link #getMediaCategories(Group)})} {
	 *     media.addAll(c.getMembers());
	 * }
	 * </code>
	 * </pre>
	 *
	 * @param group
	 *            the provided Group
	 * @return a <code>Set</code> of all accessible {@link MediaCategory} for
	 *         this {@link Group}
	 */
	public Set<MediaCategory> getMediaCategories(Group group);

	public MediaEntry getMediaEntry(String code);

	public void borrow(String code, String account);

	public void takeBack(String code);

	public void takeBack(MediaEntry entry);

	public void takeBackAll(String code);

	public void takeBackAll(MediaEntry entry);

	public MediaEntry[] fetchEntriesForPerson(String student);

}
