package obook.source.manager;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import obook.source.database.DatabaseConnector;
import obook.source.model.Media;
import obook.source.model.MediaEntry;
import obook.source.target.account.Account;

public class HibernateMediaManager {

	private static Log log = LogFactory.getLog(HibernateMediaManager.class);

	// Singleton
	private static HibernateMediaManager instance = new HibernateMediaManager();

	/**
	 * Protected Constructor.
	 */
	protected HibernateMediaManager() {

	}

	/**
	 * Returns the {@link HibernateMediaManager} instance.
	 * 
	 * @return
	 */
	public static HibernateMediaManager getInstance() {
		return instance;
	}

	/**
	 * 
	 * @param media
	 */
	public int register(Media media) {
		Session session = DatabaseConnector.factory.openSession();
		Transaction tx = null;
		Integer id = null;
		try {
			tx = session.beginTransaction();
			id = (Integer) session.save(media);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			log.warn("Media component " + media.getID() + " cannot be registered: " + e.getMessage());
		} finally {
			session.close();
		}
		log.info("Media component " + media.getID() + " has successfully been registered");
		return id;
	}

	/**
	 * 
	 * @param entry
	 */
	public int register(MediaEntry entry) {
		Session session = DatabaseConnector.factory.openSession();
		Transaction tx = null;
		Integer id = null;
		try {
			tx = session.beginTransaction();
			id = (Integer) session.save(entry);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return id;
	}

	/**
	 * 
	 * @param lookup
	 */
	public void update(Media media) {
		Session session = DatabaseConnector.factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(media);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/**
	 * 
	 * @param lookup
	 */
	public void update(MediaEntry entry) {
		Session session = DatabaseConnector.factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(entry);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/**
	 * 
	 * @param lookup
	 */
	public void delete(Media media) {
		Session session = DatabaseConnector.factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// TODO delete all related MediaEntries
			session.delete(media);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/**
	 * 
	 * @param lookup
	 */
	public void delete(MediaEntry entry) {
		Session session = DatabaseConnector.factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(entry);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/**
	 * 
	 * @param media
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MediaEntry[] fetchForMedia(String mediaID) {
		Session session = DatabaseConnector.factory.openSession();
		Transaction tx = null;
		List<MediaEntry> entries = null;
		try {
			tx = session.beginTransaction();
			entries = session.createQuery("FROM MediaEntries").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return entries.toArray(new MediaEntry[0]);
	}

	/**
	 * 
	 * @param entry
	 * @param person
	 */
	public void borrow(String mediaID, String personID) {
		Session session = DatabaseConnector.factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			MediaEntry entry = session.load(MediaEntry.class, mediaID);
			Account person = session.load(Account.class, personID);
			entry.setTarget(person);
			entry.addAction(null); // TODO Media Borrowed Action
			session.update(entry);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void takeBack(MediaEntry entry) {
		// TODO
	}

	public void takeBackAll(MediaEntry entry) {
		// TODO
	}

	/**
	 * 
	 * @param student
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public MediaEntry[] fetchForPerson(String studentID) {
		Session session = DatabaseConnector.factory.openSession();
		Transaction tx = null;
		List<MediaEntry> entries = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM MediaEntries WHERE person = :id");
			query.setParameter("id", studentID);
			entries = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return entries.toArray(new MediaEntry[0]);
	}

}
