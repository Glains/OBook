
/*
 * (C) Copyright 2016 Lukas Kannenberg
 */

package obook.source.manager;

import java.util.Set;

import obook.source.model.Group;
import obook.source.model.Media;
import obook.source.target.account.Account;

/**
 * // TODO insert type description here
 *
 * @author Lukas Kannenberg
 * @since 2016-07-20
 * @version 0.0.1
 *
 */
public interface GroupManager {

	// ----------------------------------------------------------
	// General
	// ----------------------------------------------------------

	public int register(Group group);

	public void update(Group group);

	public void delete(Group group);

	// ----------------------------------------------------------
	// Group Queries
	// ----------------------------------------------------------

	/**
	 * Loads and returns the Group with the specified name from the database. If
	 * no match has been found, null will be returned.
	 *
	 * @param name
	 *            the name of the Group
	 * @return the Group with the specified name, null if no match has been
	 *         found
	 */
	public Group getGroupByName(String name);

	/**
	 * Loads and returns a Set of all accessible Media for the provided Group.
	 * This includes Media that have been filtered by a provided black- or
	 * whitelist of the Group.
	 *
	 * @param group
	 *            the name of the Group
	 * @return a Set of all Media that are accessible for the provided Group,
	 *         null if the Group is null
	 */
	public Set<Media> getAccessibleMedia(String group);

	public Set<Account> getAccessibleMembers(String group);

	public boolean hasMemberWithName(String group, String account);
	
}
