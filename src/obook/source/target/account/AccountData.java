package obook.source.target.account;

import java.util.ArrayList;
import java.util.List;

import obook.source.persistent.PersistentObject;

@SuppressWarnings("unused")
public class AccountData extends PersistentObject {
	
	//----------------------------------------------------------
	// Static Fields
	//----------------------------------------------------------

	//----------------------------------------------------------
	// Fields
	//----------------------------------------------------------
	
	/*
	 * The last name
	 */
	private String name;
	
	/*
	 * The first name
	 */
	private String firstName;
	
	/*
	 * List of addresses
	 */
	private List<Address> addresses = new ArrayList<>();
	
	/*
	 * List of emails
	 */
	private List<String> emails = new ArrayList<>();

	//----------------------------------------------------------
	// Constructors
	//----------------------------------------------------------

	//----------------------------------------------------------
	// Methods
	//----------------------------------------------------------

	//----------------------------------------------------------
	// Accessors
	//----------------------------------------------------------

}
