package obook.source.target.account;

public class Address {

	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	/*
	 * The street including the street number
	 */
	private String street;

	/*
	 * The postal code
	 */
	private String postalCode;

	/*
	 * The town
	 */
	private String town;

	/*
	 * The country
	 */
	private String country;

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------

	/**
	 * Creates a new Address.
	 * 
	 * @param street the street
	 * @param postalCode the postal code
	 * @param town the town
	 * @param country the country
	 */
	public Address(String street, String postalCode, String town, String country) {
		super();
		this.street = street;
		this.postalCode = postalCode;
		this.town = town;
		this.country = country;
	}

	// ----------------------------------------------------------
	// Methods
	// ----------------------------------------------------------

	// ----------------------------------------------------------
	// Accessors
	// ----------------------------------------------------------

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the town
	 */
	public String getTown() {
		return town;
	}

	/**
	 * @param town the town to set
	 */
	public void setTown(String town) {
		this.town = town;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

}
