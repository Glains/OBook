package obook.services.mail.validation;

import javax.mail.MessagingException;

import obook.services.mail.MailObject;

@FunctionalInterface
public interface MailValidator {
	
	void validate(MailObject mail) throws MessagingException;
	
}
