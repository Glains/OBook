package obook.services.mail.validation;

import javax.mail.MessagingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import obook.services.mail.MailObject;

public class StandardValidator implements MailValidator {

	// Logging
	private static Log log = LogFactory.getLog(StandardValidator.class);

	// ----------------------------------------------------------
	// Methods
	// ----------------------------------------------------------

	public void validate(MailObject mail) throws MessagingException {
		if (mail == null)
			return;
		if (mail.getConfig().getHost().isEmpty()) {
			throw new MessagingException("Mail could not be send: Host was not provided.");
		} else if (mail.getFrom().isEmpty()) {
			throw new MessagingException("Mail could not be send: Sender�s ID was not provided.");
		} else if (mail.getRecipients().size() == 0) {
			throw new MessagingException("Mail could not be send: No Recipient was provided.");
		} else if (mail.getSubject().isEmpty()) {
			log.info("Warning: Mail does not include a subject.");
		} else if (mail.getContent().isEmpty()) {
			log.info("Warning: Mail does not include any content.");
		}
	}

}
