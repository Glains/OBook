package obook.services.mail.validation;

import java.util.HashMap;

public class MailValidators {

	// ----------------------------------------------------------
	// Static Fields
	// ----------------------------------------------------------

	// Default Implementations
	public static final MailValidator STANDARD = new StandardValidator();

	//
	private static HashMap<String, MailValidator> validators = new HashMap<>();

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------

	private MailValidators() {
			
	}

	// ----------------------------------------------------------
	// Static Methods
	// ----------------------------------------------------------
	
	/**
	 * 
	 * @param validator
	 */
	public static void register(String name, MailValidator validator) {
		validators.put(name, validator);
	}
	
	/**
	 * 
	 * @param name
	 */
	public static MailValidator load(String name) {
		return validators.get(name);
	}

}
