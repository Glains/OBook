package obook.services.mail;

import java.util.HashMap;
import java.util.Objects;

import obook.services.mail.config.MailConfiguration;
import obook.services.mail.validation.StandardValidator;

/**
 * TODO Class Description
 *
 * @author Lukas
 * @since 19.03.2016
 * @version 0.0.1
 *
 */
public class MailFactory {

	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	//
	private HashMap<String, MailObject> copys = new HashMap<>();
	
	// The configuration for MailObjects
	private MailConfiguration config;

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------

	/**
	 * 
	 * @param config
	 */
	public MailFactory(MailConfiguration config) {
		this.config = Objects.requireNonNull(config, "The Configuration cannot be null.");
	}

	// ----------------------------------------------------------
	// Methods
	// ----------------------------------------------------------
	
	/**
	 * 
	 * @return
	 */
	public MailObject empty() {
		return new MailObject(config, new StandardValidator());
	}
	
	/**
	 * 
	 * @param name
	 * @param object
	 */
	public void save(String name, MailObject object) {
		copys.put(name, object);
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public MailObject loadObject(String name) {
		return new MailObject(copys.get(name));
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public MailBuilder loadBuilder(String name) {
		return new MailBuilder(copys.get(name));
	}

	// ----------------------------------------------------------
	// Accessors
	// ----------------------------------------------------------

	/**
	 * 
	 * @return
	 */
	public MailConfiguration getConfig() {
		return config;
	}

}
