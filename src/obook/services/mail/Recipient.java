package obook.services.mail;

import javax.mail.Message.RecipientType;

public class Recipient {

	private String address;
	private RecipientType type;

	public Recipient(String address, RecipientType type) {
		setAddress(address);
		setType(type);
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the type
	 */
	public RecipientType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(RecipientType type) {
		this.type = type;
	}
	
}
