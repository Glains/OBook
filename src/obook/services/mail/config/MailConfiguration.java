package obook.services.mail.config;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class MailConfiguration {

	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	// The host
	private String host = "";

	// Authenticator
	private Authenticator auth;

	// Get system properties
	private Properties properties = System.getProperties();

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------

	public MailConfiguration(String host) {
		setHost(host);
	}

	// ----------------------------------------------------------
	// Methods
	// ----------------------------------------------------------

	/**
	 * If an authentication is required by the mail server/provider you may
	 * specify you username and password.
	 * 
	 * @param user the username
	 * @param password the password
	 */
	public MailConfiguration authenticate(String user, String password) {
		auth = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, password);
			}
		};
		return this;
	}

	/**
	 * 
	 * @param option
	 */
	public MailConfiguration withPropertiesFor(MailSecurityOption option) {
		switch (option) {
		case STARTTLS:
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.host", host);
			properties.put("mail.smtp.port", "587");
			break;
		case SSL:
			properties.put("mail.smtp.host", host);
			properties.put("mail.smtp.socketFactory.port", "465");
			properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.port", "465");
			break;
		default:
			break;
		}
		return this;
	}
	
	/**
	 * 
	 * @param properties
	 * @return
	 */
	public MailConfiguration withProperty(String key, String value) {
		properties.setProperty(key, value);
		return this;
	}

	// ----------------------------------------------------------
	// Accessors
	// ----------------------------------------------------------

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	private void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the auth
	 */
	public Authenticator getAuth() {
		return auth;
	}

	/**
	 * @return the properties
	 */
	public Properties getProperties() {
		return properties;
	}

}
