package obook.services.mail.config;

public enum MailSecurityOption {

	STARTTLS,
	SSL;
	
}
