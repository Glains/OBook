package obook.services.mail.config;

import java.util.HashMap;

public class MailConfigurations {

	// ----------------------------------------------------------
	// Static Fields
	// ----------------------------------------------------------

	private static HashMap<String, MailConfiguration> configurations = new HashMap<>();

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------
	
	private MailConfigurations() {
		
	}

	// ----------------------------------------------------------
	// Static Methods
	// ----------------------------------------------------------

	/**
	 * 
	 * @param name
	 * @param config
	 */
	public static void register(String name, MailConfiguration config) {
		configurations.put(name, config);
	}
	
	/**
	 * 
	 * @param name
	 * @return
	 */
	public static MailConfiguration load(String name) {
		return configurations.get(name);
	}

}
