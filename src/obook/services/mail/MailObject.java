package obook.services.mail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import obook.services.mail.config.MailConfiguration;
import obook.services.mail.validation.MailValidator;
import obook.services.mail.validation.MailValidators;
import obook.services.mail.validation.StandardValidator;

public class MailObject {

	// Logging
	private static Log log = LogFactory.getLog(MailObject.class);

	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	// Sender's email ID
	private String from = "";

	// Recipient's email ID
	private List<Recipient> recipients = new ArrayList<>();

	// The subject to use
	private String subject = "";

	// The content of this mail
	private String content = "";

	// Attachments
	private List<String> attachments = new ArrayList<>();

	// Get the default Session object.
	private Session session;

	// Validator
	private MailValidator validator;

	// Configuration
	private MailConfiguration config;

	// ----------------------------------------------------------
	// Constructor
	// ----------------------------------------------------------

	/**
	 * Private Constructor.
	 */
	protected MailObject() {

	}

	/**
	 * Constructs a new empty MailObject provided with a MailValidator. The
	 * MailValidator may be customized by implementing the {@link MailValidator}
	 * Interface to validate MailObjects. If no MailValidator is provided, the
	 * {@link StandardValidator} will be used instead.
	 * 
	 * @param validator
	 */
	public MailObject(MailConfiguration config, MailValidator validator) {
		this.config = Objects.requireNonNull(config, "The Configuration cannot be null.");
		this.validator = (validator != null) ? validator : MailValidators.STANDARD;
	}

	/**
	 * 
	 * @param copy
	 */
	public MailObject(MailObject copy) {
		this.config = Objects.requireNonNull(copy.getConfig(), "The Configuration cannot be null.");
		this.validator = (copy.getValidator() != null) ? copy.getValidator() : MailValidators.STANDARD;
		setFrom(copy.getFrom());
		this.recipients.addAll(copy.getRecipients());
		setSubject(copy.getSubject());
		setContent(copy.getContent());
		this.attachments.addAll(copy.getAttachments());
	}

	// ----------------------------------------------------------
	// Methods
	// ----------------------------------------------------------

	/**
	 * Performs setup operations on this MailObject.
	 */
	private void createSession() {
		session = Session.getDefaultInstance(config.getProperties(), config.getAuth());
	}

	/**
	 * Sends this mail to the listed recipients, including all provided
	 * information. Before sending, the MailObject will be validated by the
	 * provided MailValidator (if none provided, the {@link StandardValidator}
	 * will be used). After successful validation, the mail will be send. In an
	 * error occurs during process, a MessagingException will be thrown.
	 * 
	 * @throws MessagingException if an error occurs during the process
	 */
	public void send() throws MessagingException {
		createSession();
		validator.validate(this);

		MimeMessage message = new MimeMessage(session);

		// Set Senders Address
		message.setFrom(new InternetAddress(from));

		// Add Recipients
		for (Recipient recipient : recipients) {
			message.addRecipient(recipient.getType(), new InternetAddress(recipient.getAddress()));
		}

		// Set Subject
		message.setSubject(subject);

		Multipart multipart = new MimeMultipart();

		BodyPart messageBodyPart = new MimeBodyPart();

		// Set Text of the message (HTML allowed)
		messageBodyPart.setContent(content, "text/html; charset=utf-8");
		multipart.addBodyPart(messageBodyPart);

		// Add Attachments
		for (String filename : attachments) {
			BodyPart attachment = new MimeBodyPart();
			DataSource source = new FileDataSource(filename);
			attachment.setDataHandler(new DataHandler(source));
			attachment.setFileName(filename);
			multipart.addBodyPart(attachment);
		}

		// Send the complete message parts
		message.setContent(multipart);

		// Send message
		Transport.send(message);
		log.info("Mail has been successfully send to " + recipients.size() + " Recipients.");
	}

	/**
	 * 
	 * 
	 * @param sender
	 * @param subject
	 * @param content
	 * @param recipients
	 */
	public void setRelatantInformation(String sender, String subject, String content, Recipient... recipients) {
		setFrom(sender);
		setSubject(subject);
		setContent(content);
		if (recipients != null)
			getRecipients().addAll(Arrays.asList(recipients));
	}

	// ----------------------------------------------------------
	// Accessors
	// ----------------------------------------------------------

	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * @return the recipients
	 */
	public List<Recipient> getRecipients() {
		return recipients;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the attachments
	 */
	public List<String> getAttachments() {
		return attachments;
	}

	/**
	 * @return the validator
	 */
	public MailValidator getValidator() {
		return validator;
	}

	/**
	 * 
	 * @return
	 */
	public MailConfiguration getConfig() {
		return config;
	}

}
