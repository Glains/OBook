package obook.services.mail;

import obook.services.mail.config.MailConfiguration;
import obook.services.mail.validation.MailValidator;

public class MailBuilder {

	// ----------------------------------------------------------
	// Fields
	// ----------------------------------------------------------

	private MailObject mail;

	// ----------------------------------------------------------
	// Constructors
	// ----------------------------------------------------------

	/**
	 * 
	 * @param config
	 */
	public MailBuilder(MailConfiguration config) {
		this.mail = new MailObject(config, null);
	}
	
	/**
	 * 
	 * @param config
	 * @param validator
	 */
	public MailBuilder(MailConfiguration config, MailValidator validator) {
		this.mail = new MailObject(config, validator);
	}
	
	/**
	 * 
	 * @param copy
	 */
	public MailBuilder(MailObject copy) {
		this.mail = new MailObject(copy);
	}

	// ----------------------------------------------------------
	// Methods
	// ----------------------------------------------------------

	/**
	 * 
	 * @param sender
	 * @return
	 */
	public MailBuilder withSender(String sender) {
		mail.setFrom(sender);
		return this;
	}

	/**
	 * 
	 * @param subject
	 * @return
	 */
	public MailBuilder withSubject(String subject) {
		mail.setSubject(subject);
		return this;
	}

	/**
	 * 
	 * @param content
	 * @return
	 */
	public MailBuilder withContent(String content) {
		mail.setContent(content);
		return this;
	}

	/**
	 * 
	 * @param recipients
	 * @return
	 */
	public MailBuilder withRecipients(Recipient... recipients) {
		for (Recipient recipient : recipients) {
			mail.getRecipients().add(recipient);
		}
		return this;
	}
	
	/**
	 * 
	 * @param paths
	 * @return
	 */
	public MailBuilder withAttachments(String... paths) {
		for (String string : paths) {
			mail.getAttachments().add(string);
		}
		return this;
	}
	
	/**
	 * 
	 * @return
	 */
	public MailObject get() {
		return mail;
	}
	
}
